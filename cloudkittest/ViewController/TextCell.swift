//
//  TextCell.swift
//  EjercicioBBDDAvanzadas
//
//  Created by Marro Gros Gabriel on 10/2/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import UIKit

class TextCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet var userIcon : UIImageView!
    @IBOutlet var messageText : UILabel!
    let imageCache = NSCache<NSString, UIImage>()
    @IBOutlet weak var loading: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        loading.isHidden = true
        weak var weakSelf = self
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut,
                       animations: {
                        weakSelf?.userIcon.alpha = 0.0
                        //weakSelf?.cellView.alpha = 0.0
                        weakSelf?.cellView.frame.origin.x = -200
        }) { done in
            
            UIView.animate(withDuration: 0.3, delay: 0.2, options: .curveEaseInOut,
                           animations: {
                            weakSelf?.userIcon.alpha = 1.0
                             //weakSelf?.cellView.alpha = 1.0
                            weakSelf?.cellView.frame.origin.x = 0
                            
            })
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func onFetchUserInfoCompletion(recordName : String) {
        weak var weakSelf = self
        loading.isHidden = false
        loading.startAnimating()
        weakSelf?.userIcon.alpha = 0.0
        CoreDataManager.retrieveImageCompletion(recordName: recordName, completionHandler: { (imageName, error) in
                guard error == nil  else {
                    return
                }
                weakSelf?.onGetImageInfoCompletion(imageName: imageName, completionHandler: { (imageURL, error) in
                    DispatchQueue.main.async {
                        weakSelf?.downloadImage(url: imageURL,imageName: imageName, imageCache: (weakSelf?.imageCache)!, completion: { (img) in
                            weakSelf?.userIcon.image = img
                            weakSelf?.loading.isHidden = true
                            UIView.animate(withDuration: 0.3, delay: 0.2, options: .curveEaseInOut,
                                           animations: {
                                            weakSelf?.userIcon.alpha = 1.0
                            })
                        })
                    }
                })
        })
    }
    


}
