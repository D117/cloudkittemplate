//
//  ViewController.swift
//  EjercicioBBDDAvanzadas
//
//  Created by Marro Gros Gabriel on 10/2/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource {

    

    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var msgTF: UITextView!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBOutlet var bottomConstraint : NSLayoutConstraint!
    
    lazy var frc : NSFetchedResultsController<Message> = {
        
        let req = NSFetchRequest<Message>(entityName:"Message")
        req.sortDescriptors = [ NSSortDescriptor(key: "creationDate", ascending: true)]
        
        let _frc = NSFetchedResultsController(fetchRequest: req,
                                              managedObjectContext: CoreDataManager.contextCore,
                                              sectionNameKeyPath: nil,
                                              cacheName: nil)
        
        _frc.delegate = self
        try? _frc.performFetch()
        return _frc
    }()
    
    var originalBottomSpace : CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalBottomSpace = bottomConstraint.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardAnimation(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardAnimation(_:)), name: .UIKeyboardWillHide, object: nil)
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    @objc func keyboardAnimation(_ notification: Notification) {
        let userInfo = notification.userInfo!
        
        let animationDuration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        let keyboardEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! CGRect)
        let convertedKeyboardEndFrame = view.convert(keyboardEndFrame, from: view.window)
        
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as! NSNumber
        
        let animationCurve = UIViewAnimationOptions(rawValue:curve.uintValue << 16)
        
        let height = view.bounds.size.height - convertedKeyboardEndFrame.origin.y
        
        bottomConstraint.constant = originalBottomSpace - height
        
        UIView.animate(withDuration: animationDuration,
                       delay: 0.0,
                       options: [.beginFromCurrentState, animationCurve],
                       animations: {
                        self.view.layoutIfNeeded()
        }, completion: nil)
        
    }


    // MARK - Table View Data Source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let section = frc.sections?[section] {
            principalView.isHidden = true
            
            return section.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message: Message = frc.object(at: indexPath)
        
        let cell: TextCell
        if message.registerName == UserName {
            cell = tableView.dequeueReusableCell(withIdentifier: cells.rightCell.rawValue, for: indexPath) as! TextCell
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: cells.leftCell.rawValue, for: indexPath) as! TextCell
        }
        cell.messageText.text = message.text!
        cell.onFetchUserInfoCompletion(recordName: message.registerName!)
        return cell
    }
    @IBAction func sendBtn() {
        
        if msgTF.text != " "{
            if msgTF.text != "" {
                CloudKitHelper.createMessage(msgTF.text)
                msgTF.text = ""
                scrollToBottom()
            }
            
        }
        
        msgTF.resignFirstResponder()
        
    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            let rows = self.tableView.numberOfRows(inSection: 0)
            let indexPath = IndexPath(row: rows-1, section: 0)
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
        }
    }
    
}

