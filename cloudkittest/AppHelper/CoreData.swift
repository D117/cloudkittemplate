//
//  CoreData.swift
//  cloudkittest
//
//  Created by Daniel Cabrera on 2/19/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import CloudKit

struct CoreDataManager {
    static  var contextCore : NSManagedObjectContext = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let persistenContainer = appDelegate.persistentContainer
        return persistenContainer.viewContext
    }()
    
    
    static func saveMessagesCompletion(record : CKRecord, recordName : String, completionHandler: @escaping (Bool, Error?) -> ()) {
        let managedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedContext.parent = CoreDataManager.contextCore
        let defaults = UserDefaults.standard
        if let date = defaults.object(forKey: LastMessage) as? Date {
            if (record["creationDate"] as? Date)! > date  {
                managedContext.perform {
                    let entity =
                        NSEntityDescription.insertNewObject(forEntityName: "Message", into: managedContext) as! Message
                    
                    if managedContext.hasChanges {
                        
                        entity.text = record["text"] as? String
                        entity.registerName = recordName
                        entity.creationDate = record["creationDate"] as? Date
                        do {
                            if managedContext.hasChanges {
                                try managedContext.save()
                                completionHandler(true,nil)
                                defaults.set(record["creationDate"] as? Date, forKey: LastMessage)
                                if let context = managedContext.parent {
                                    context.perform {
                                        try? context.save()
                                        
                                    }
                                }
                            }
                            
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                            completionHandler(false,error)
                        }
                    }
                    
                }
            }
        }
    }
    
    static func saveProfileCompletion(record : CKRecord, recordName : String, completionHandler: @escaping (Bool, Error?) -> ()) {
            let managedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            managedContext.parent = CoreDataManager.contextCore
                if !CoreDataManager.userRecordExists(managedContext: managedContext, recordName: recordName){
                
                            let entity =
                                NSEntityDescription.insertNewObject(forEntityName: "User", into: managedContext) as! User
                            CoreDataManager.saveUserImage(image: record["image"] as? CKAsset, recordName: recordName, completionHandler: { (haveImage, error) in
                                if haveImage {
                                    entity.image = "\(recordName).png"
                                }
                            })
                            entity.fullName = record["fullName"] as? String
                            entity.registerName = recordName
                            
                            do {
                                if managedContext.hasChanges {
                                    try managedContext.save()
                                    completionHandler(true, nil)
                                    if let context = managedContext.parent {
                                            context.perform {
                                                try? context.save()
                                            }
                                    }
                                }
                                
                            } catch let error as NSError {
                                print("Could not save. \(error), \(error.userInfo)")
                                completionHandler(false, error)
                            }
                            
                        } else {
                            completionHandler(true, nil)
                        }
        
    }
    
    static func saveUserImage(image: CKAsset?, recordName : String, completionHandler: @escaping (Bool, Error?) -> ()) {
        if let asset = image
        {
            
            let data = try? Data(contentsOf: asset.fileURL)
            let image = UIImage(data: data!)
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let imageURL = documentsURL?.appendingPathComponent("\(recordName).png")
            
            do {
                try UIImagePNGRepresentation(image!)?.write(to: imageURL!)
                //entity.image = "\(recordName).png"
                completionHandler(true,nil)
            } catch {
                completionHandler(false,error)
            }
        }
    }
    
    
    
    static func retrieveImageCompletion(recordName : String, completionHandler: @escaping (String, Error?) -> ()) {
            let managedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            managedContext.parent = CoreDataManager.contextCore
            managedContext.perform {
                let user = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
                user.predicate = NSPredicate(format: "registerName == %@", recordName)
                
                do {
                    let fetchedEmployees = try managedContext.fetch(user) as! [User]
                    if fetchedEmployees.count > 0 {
                        if let img = fetchedEmployees[0].image {
                            completionHandler(img, nil)
                        }
                    }
                    
                } catch {
                    fatalError("Failed to fetch User: \(error)")
                }
                
                completionHandler("", nil)
            }
    }
    
    
    static func retrieveFullNameCompletion(recordName : String, completionHandler: @escaping ([User], Bool) -> ()) {
        let managedContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedContext.parent = CoreDataManager.contextCore
        managedContext.perform {
            let user = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            user.predicate = NSPredicate(format: "registerName == %@", recordName)
            
            do {
                let fetchedEmployees = try managedContext.fetch(user) as! [User]
                if fetchedEmployees.count > 0 {
                        completionHandler(fetchedEmployees, true)
                }
                
            } catch {
                fatalError("Failed to fetch User: \(error)")
            }
            
        }
    }

    
    static func userRecordExists(managedContext :  NSManagedObjectContext, recordName : String) -> Bool {
        let user = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        user.predicate = NSPredicate(format: "registerName == %@", recordName)
        do {
            let fetchedEmployees = try managedContext.fetch(user) as! [User]
            if fetchedEmployees.count > 0 {
                return true
            } else {
                return false
            }
            
            
        } catch {
            fatalError("Failed to fetch User: \(error)")
        }
        return true
    }
    
    

}
