//
//  CloudKitHelper.swift
//  cloudkittest
//
//  Created by Marro Gros Gabriel on 9/2/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import Foundation
import CloudKit
import UIKit
import CoreData
struct CloudKitHelper {
    
    static let subscriptionName = "MessageChanges"
    static var publicDatabase : CKDatabase {
        let container = CKContainer.default()
        return container.publicCloudDatabase
    }
    
    
    /// Asigna un valor "fullName" al campo "fullName" del registro de usuario actual
    ///
    /// - Parameter fullName: Nuevo nombre de usuario
    /// - Parameter imgName: Nuevo Imagen de perfil
    static func setMyFullName(_ fullName: String, _ image: UIImage?) {
        CKContainer.default().fetchUserRecordID { (recordID, error) in
            guard error == nil else {
                debugPrint("Error \(error!.localizedDescription)")
                return
            }
            if let userID = recordID {
                debugPrint("I am \( userID.recordName )")
                
                publicDatabase.fetch(withRecordID: userID, completionHandler: { (record, error) in
                    guard error == nil,
                            record != nil else {
                                debugPrint("Cannot get my own user record")
                        return
                    }
                    
                    if let image = image {
                        let asset = CloudKitHelper.createImageCloud(image: image)
                        CoreDataManager.saveUserImage(image: asset, recordName: UserName, completionHandler: { (save, error) in
                            record!["image"] = asset
                        })
                    }
                    record!["fullName"] = fullName as NSString
                    
                    publicDatabase.save(record!, completionHandler: { (record, error) in
                        if error != nil {
                            debugPrint("Message creation error: \( error!.localizedDescription )")
                            return
                        }
                        
                        debugPrint("User saved with id \( record?.recordID.recordName ?? "no-name" )")
                    })
                    
                })
            }
        }
    }
    
    static func createImageCloud(image: UIImage) -> CKAsset {
        let documentsDirectoryPath:NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let tempImageName = "perfil.jpg"
        
            let imageData:Data = UIImageJPEGRepresentation(image, 1.0)!
            let path:String = documentsDirectoryPath.appendingPathComponent(tempImageName)
            try? UIImageJPEGRepresentation(image, 1.0)!.write(to: URL(fileURLWithPath: path), options: [.atomic])
            let imageURL = URL(fileURLWithPath: path)
            try? imageData.write(to: imageURL, options: [.atomic])
            
            let File:CKAsset?  = CKAsset(fileURL: URL(fileURLWithPath: path))
            return File!
    }
    

    /// Crea en CloudKit un nuevo mensaje con un texto.
    ///
    /// - Parameter text: Texto del mensaje a enviar al chat
    static func createMessage( _ text: String) {
        CKContainer.default().fetchUserRecordID { (recordID, error) in
            guard error == nil else {
                debugPrint("Error \(error!.localizedDescription)")
                return
            }
            let message = CKRecord(recordType: "Message")
            message["text"] = text as NSString
            message["registerName"] = recordID!.recordName as NSString
            publicDatabase.save(message) { (record, error) in
                if error != nil {
                    debugPrint("Message creation error: \( error!.localizedDescription )")
                    return
                }
                    
                    CoreDataManager.saveMessagesCompletion(record: message, recordName:  "__defaultOwner__", completionHandler: { (save, error) in
                        guard error == nil else {
                            return
                        }
                        
                    })
                debugPrint("Message saved with id \( record?.recordID.recordName ?? "no-name" )")
            }
        }
        
        
    }
    
    /// Subscripción para recibir notificaciones push cuando hay cambios en CloudKit
    static func subscribeToChanges() {
        
        publicDatabase.fetchAllSubscriptions { (subscriptions, error) in
            if error != nil {
                debugPrint("Error \(error!.localizedDescription )")
                return
            }
            
            if subscriptions?.count ?? 0 == 0 {
                saveSubscription()
            }
 
        }
    }
    
    private static func saveSubscription() {
        
        let options:CKQuerySubscriptionOptions
        options = [.firesOnRecordUpdate,.firesOnRecordCreation,.firesOnRecordDeletion]
        
        let predicate = NSPredicate(format: "TRUEPREDICATE")
        
        let subscription = CKQuerySubscription(recordType: "Message",
                                               predicate: predicate,
                                               subscriptionID: "mySubscription",
                                               options: options)
        
        let info = CKNotificationInfo()
        info.soundName = "chan.aiff"
        info.alertBody = "New message"
        
        subscription.notificationInfo = info
        
        publicDatabase.save(subscription) { (subscription, error) in
            if error != nil {
                debugPrint("Error saving subscription \( error!.localizedDescription)")
                return
            }
        }
    }
    
    /// Ejemplo de query para buscar mensajes.
    static func queryForMessages(from date: Date = Date(timeIntervalSince1970: 0.0)) {
        
        let predicate = NSPredicate(format: "creationDate >= %@", date as NSDate )
        let query = CKQuery(recordType: "Message", predicate: predicate)
        
        publicDatabase.fetchAllRecordZones { (zones, error) in
            if error != nil {
                debugPrint("Error, no zones \( error!.localizedDescription )")
                return
            }
            
            if let zone = zones?.first {
                publicDatabase.perform(query, inZoneWith: zone.zoneID,
                                       completionHandler: { (records, error) in
                                        
                                        if error != nil {
                                            debugPrint("Error query \( error!.localizedDescription )")
                                            return
                                        }
                                        guard records != nil else { return }
                                        
                                        for record in records! {
                                            debugPrint("Mensaje : \( (record["text"] as? String) ?? "no-tesxt" )")
                                            
                                            if let messageDate = record["creationDate"] as? Date {
                                                debugPrint("Fecha del mensaje \( messageDate.description )")
                                                
                                                // Y podemos acceder a la información del registro del usuario que creó el mensaje:
                                                if let recordID = record.creatorUserRecordID {
                                                    debugPrint("Autor id \(recordID.recordName)")
                                                    
                                                    if recordID.recordName == "__defaultOwner__" {
                                                        debugPrint("I am the author of the message")
                                                        
                                                    } else {
                                                        debugPrint("The author is ")
                                                    }
                                                    debugPrint("------")
                                                    // A partir del recordID del usuario, podemos acceder (asíncronamente) a
                                                    // registro de usuario del sistema
                                                    // 1
                                                    //fetchUser(recordID,  recordName: recordID.recordName)
                                                    
                                                    CoreDataManager.saveMessagesCompletion(record: record, recordName: recordID.recordName, completionHandler: { (save, error) in
                                                        if save {
                                                            fetchUserCompletion(recordID, recordName: recordID.recordName, completionHandler: { (isSave, error) in
                                                                guard error == nil else {
                                                                    return
                                                                }
                                                            })
                                                        }
                                                    })
                                                    
                                                }
                                            }
                                        }
                })
            }
            
        }
    }
    /// EJEMPLO:
    /// Acceso al registro de un usuario. La tabla de usuarios del sistema NO ES BUSCABLE. O sea
    /// que no se pueden hacer QUERYs y solo se puede acceder de forma directa con el recordID.
    ///
    /// - Parameter user: recordID del registro de usuario
    
    static func fetchUserCompletion(_ user: CKRecordID,  recordName: String,  completionHandler: @escaping (Bool, Error?) -> ()) {
        publicDatabase.fetch(withRecordID: user) { (record, error) in
            
            guard error == nil,
                record != nil else {
                    debugPrint("Error fetching user \(error?.localizedDescription ?? "no-error-info" )")
                    completionHandler(false, error)
                    return
            }
            //cDM.saveProfile(record: record!, recordName: recordName)
            CoreDataManager.saveProfileCompletion(record: record!, recordName: recordName, completionHandler: { (save, error) in
                debugPrint(save)
                completionHandler(save,error)
            })
            debugPrint("User record \( (record!["fullName"] as? String) ?? "no-fullname") ")
        }
    
    }

}
