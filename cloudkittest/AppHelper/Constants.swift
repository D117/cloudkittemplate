//
//  Constants.swift
//  cloudkittest
//
//  Created by Daniel Cabrera on 2/18/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import Foundation

enum cells : String {
    case leftCell
    case rightCell
}

var UserName: String = "__defaultOwner__"

var LastMessage : String = "LastMessage"
