//
//  UIImage+Extension.swift
//  cloudkittest
//
//  Created by Daniel Cabrera on 2/20/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import Foundation
import UIKit
extension UITableViewCell {
    
    func onGetImageInfoCompletion(imageName: String, completionHandler: @escaping (URL, Error?) -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if imageName != "" {
                
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(imageName)
                    completionHandler(imageURL, nil)
                }
            }
        }
        
    }
    
    func downloadImage(url: URL,imageName: String, imageCache: NSCache<NSString, UIImage>, completion: @escaping (UIImage?) -> Void) {
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        } else {
            onGetImageInfoCompletion(imageName: imageName, completionHandler: { (imageURL, error) in
                guard error == nil else {
                    return
                }
                if let image = UIImage(contentsOfFile: imageURL.path) {
                    imageCache.setObject(image, forKey: imageURL.absoluteString as NSString)
                    completion(image)
                }
            })
        }
    }
}


extension UIViewController {
    
    func onGetImageInfoCompletion(imageName: String, completionHandler: @escaping (URL, Error?) -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if imageName != "" {
                
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent(imageName)
                    completionHandler(imageURL, nil)
                }
            }
        }
        
    }
    
    func downloadImage(url: URL,imageName: String, imageCache: NSCache<NSString, UIImage>, completion: @escaping (UIImage?) -> Void) {
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            completion(cachedImage)
        } else {
            onGetImageInfoCompletion(imageName: imageName, completionHandler: { (imageURL, error) in
                guard error == nil else {
                    return
                }
                if let image = UIImage(contentsOfFile: imageURL.path) {
                    imageCache.setObject(image, forKey: imageURL.absoluteString as NSString)
                    completion(image)
                }
            })
        }
    }
}
