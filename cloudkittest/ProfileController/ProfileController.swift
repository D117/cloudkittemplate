//
//  ProfileController.swift
//  cloudkittest
//
//  Created by Daniel Cabrera on 2/19/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import UIKit

class ProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var profileImage: UIButton!
    @IBOutlet weak var profileName: UITextField!
    let picker = UIImagePickerController()
    let imageCache = NSCache<NSString, UIImage>()
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onFetchUserInfo(recordName: UserName)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func chooseProfileImg() {
        picker.allowsEditing = true
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    @IBAction func saveProfile() {
        CloudKitHelper.setMyFullName(profileName.text!, profileImage.imageView?.image)
        _ = navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerEditedImage] as! UIImage
        profileImage.setImage(chosenImage, for: .normal)
        dismiss(animated:true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
   func onFetchUserInfo(recordName : String) {
    CoreDataManager.retrieveFullNameCompletion(recordName: recordName) { (user, bool) in
        guard bool == true else {
            return
        }
        if let image : String = user[0].image {
            self.onGetImageInfoCompletion(imageName: image, completionHandler: { (imageURL, error) in
                DispatchQueue.main.async {
                    self.downloadImage(url: imageURL,imageName: image, imageCache: self.imageCache, completion: { (img) in
                        self.profileImage.setImage(img, for: .normal)
                    })
                }
            })
        }
            if let fName = user[0].fullName {
                DispatchQueue.main.async {
                    self.profileName.text = fName
                }
            }
        }
    }
}
