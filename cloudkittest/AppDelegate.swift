//
//  AppDelegate.swift
//  cloudkittest
//
//  Created by Marro Gros Gabriel on 7/2/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let defaults = UserDefaults.standard
    var lastDate = Date(timeIntervalSince1970: 0.0)

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Subscribe to changes in CloudKit to receive push notifications
        CloudKitHelper.subscribeToChanges()
        
        // Check if launched from notification center
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            
            let aps = notification["aps"] as! [String: AnyObject]
            debugPrint("Notification received: \(aps)")
            
            
        }
        UNUserNotificationCenter.current().delegate = self
        registerForPushNotifications()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        if let date = defaults.object(forKey: LastMessage) as? Date {
            lastDate = date
        } else {
            defaults.set(lastDate, forKey: LastMessage)
        }
        
        debugPrint("sync \(lastDate)")
        
        CloudKitHelper.queryForMessages(from: lastDate)
       
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "cloudkittest")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        debugPrint("Device Token: \(token)")
        // Subscribe to changes in CloudKit to receive push notifications
        CloudKitHelper.subscribeToChanges()
    }
    
    
    // MARK - UNuserNotificationCenterDelegate protocol
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let options = UNNotificationPresentationOptions.sound
        completionHandler(options)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        completionHandler()
        if let date = defaults.object(forKey: LastMessage) as? Date {
            lastDate = date
        }
        
        debugPrint("sync \(lastDate)")
        
        CloudKitHelper.queryForMessages(from: lastDate)
    }
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (success, error) in
            DispatchQueue.main.async {
                
                UIApplication.shared.registerForRemoteNotifications()
            }
            
        }
    }
}

