//
//  cloudkittestTests.swift
//  cloudkittestTests
//
//  Created by Marro Gros Gabriel on 14/2/18.
//  Copyright © 2018 Marro Gros Gabriel. All rights reserved.
//

import XCTest
import CloudKit
import UIKit
import CoreData


class cloudkittestTests: XCTestCase {
    

    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testProfile() {
        let expect = expectation(description: "setFullName")
        CKContainer.default().fetchUserRecordID { (recordID, error) in
            let fullName = "Daniel Cabrera"
            guard error == nil else {
                debugPrint("Error \(error!.localizedDescription)")
                XCTFail("Message creation error: \( error!.localizedDescription )")
                return
            }
            if let userID = recordID {
                debugPrint("I am \( userID.recordName )")
                
                CloudKitHelper.publicDatabase.fetch(withRecordID: userID, completionHandler: { (record, error) in
                    guard error == nil,
                        record != nil else {
                            debugPrint("Cannot get my own user record")
                            XCTFail("Cannot get my own user record")
                            return
                    }
                    
                    if let image = UIImage(named: "avatar") {
                        record!["image"] = CloudKitHelper.createImageCloud(image: image)
                    }
                    record!["fullName"] = fullName as NSString
                    
                    CloudKitHelper.publicDatabase.save(record!, completionHandler: { (record, error) in
                        if error != nil {
                            debugPrint("Message creation error: \( error!.localizedDescription )")
                            XCTFail("Message creation error: \( error!.localizedDescription )")
                            return
                        }
                        
                        debugPrint("User saved with id \( record?.recordID.recordName ?? "no-name" )")
                        expect.fulfill()
                    })
                    
                })
            }
        }
        
        waitForExpectations(timeout: 10.0, handler: { (error) in
            print("Error: \(String(describing: error?.localizedDescription))")
        })
        
    }
    
    func testGetRecordID() {
        let expect = expectation(description: "getRecordID")
        CKContainer.default().fetchUserRecordID { (recordID, error) in
            guard error == nil else {
                debugPrint("Error \(error!.localizedDescription)")
                XCTFail("Message creation error: \( error!.localizedDescription )")
                return
            }
            
            CloudKitHelper.publicDatabase.fetch(withRecordID: recordID!) { (record, error) in
                guard error == nil,
                    record != nil else {
                        XCTFail("Error fetching user \(error?.localizedDescription ?? "no-error-info" )")
                        return
                }
                
                XCTAssertEqual(record!["fullName"] as? String, "Daniel Cabrera", "No es Daniel Cabrera")
                expect.fulfill()
            }
        }
        waitForExpectations(timeout: 10.0, handler: { (error) in
            print("Error: \(String(describing: error?.localizedDescription))")
        })
    }

    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testCoreDatafetch() {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                XCTFail("Fallo")
                return
        }
        
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Message")
        
        //3
        do {
            let message = try managedContext.fetch(fetchRequest)
            
            XCTAssert(message.count > 0, "No hay data")
            print("cuanta Data hay \(String(message.count))")
            
            
            XCTAssertEqual(message[0].value(forKey: "text") as? String, "Yeah!!!", "No es igual a Rio de la Plata")
            print(message[0].value(forKey: "text") as? String ?? " ")
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            XCTFail("Fallo")
        }
        
    }
    
}
